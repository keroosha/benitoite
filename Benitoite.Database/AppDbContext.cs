using System;
using System.Threading.Tasks;
using Benitoite.Database.Configuration;
using Benitoite.Database.Models.UserManagement;
using FluentMigrator.Runner;
using LinqToDB;
using LinqToDB.Data;
using LinqToDB.DataProvider;
using LinqToDB.DataProvider.PostgreSQL;
using Microsoft.Extensions.Options;

namespace Benitoite.Database;

  public class AppDbContext : DataConnection
    {
        public ITable<User> Users => this.GetTable<User>();
        public ITable<RegistrationRequest> RegistrationRequests => this.GetTable<RegistrationRequest>();

        public AppDbContext(string connectionString, IDataProvider provider) : base(provider, connectionString)
        {
        }
    }

    public interface IAppDbConnectionFactory
    {
        public AppDbContext GetConnection();
    }

    public class AppDbConnectionFactory : IAppDbConnectionFactory
    {
        private readonly IOptions<DatabaseConfig> _config;

        public AppDbConnectionFactory(IOptions<DatabaseConfig> config)
        {
            _config = config;
        }
        
        public AppDbContext GetConnection() => new(_config.Value.ConnectionString, PostgreSQLTools.GetDataProvider());
    }

    public class AppDbContextManager : DbContextManagerBase<AppDbContext>
    {
        private readonly IOptions<DatabaseConfig> _options;

        public AppDbContextManager(IAppDbConnectionFactory factory, IOptions<DatabaseConfig> options) 
            : base(factory.GetConnection)
        {
            _options = options;
        }
    }

    public class DbContextManagerBase<TContext> where TContext : DataConnection
    {
        private readonly Func<TContext> _factory;

        public DbContextManagerBase(Func<TContext> factory)
        {
            _factory = factory;
        }
        
        public async Task<T> ExecAsync<T>(Func<TContext, Task<T>> cb)
        {
            using (var ctx = _factory())
            {
                return await cb(ctx);
            }
        }

        public async Task ExecAsync(Func<TContext, Task> cb)
        {
            using (var ctx = _factory())
            {
                await cb(ctx);
            }
        }
        
        public async Task<T> ExecWithTransaction<T>(Func<TContext, Task<T>> cb)
        {
            using (var ctx = _factory())
            {
                using var trx = await ctx.BeginTransactionAsync();
                var res = await cb(ctx);
                await trx.CommitAsync();
                return res;
            }
        }

        public async Task ExecWithTransaction(Func<TContext, Task> cb)
        {
            using (var ctx = _factory())
            {
                using var trx = await ctx.BeginTransactionAsync();
                await cb(ctx);
                await trx.CommitAsync();
            }
        }
    }