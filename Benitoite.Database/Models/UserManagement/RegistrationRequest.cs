
using System;
using LinqToDB.Mapping;

namespace Benitoite.Database.Models.UserManagement;

public enum RegistrationStatus
{
    New,
    Approved,
    Rejected
}

[Table(TableName)]
public class RegistrationRequest
{
    public const string TableName = "RegistrationRequests";
    [PrimaryKey] public Guid Id { get; set; }
    [Column] public string Email { get; set; }
    [Column] public string Name { get; set; }
    [Column] public RegistrationStatus Status { get; set; }
}