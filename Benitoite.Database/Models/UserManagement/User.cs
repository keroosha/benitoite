using System;
using LinqToDB.Mapping;

namespace Benitoite.Database.Models.UserManagement;

[Table(TableName)]
public record User
{
    public const string TableName = "Users";
    [PrimaryKey] public Guid Id { get; set; }
    
    [Column] public string? Email { get; set; }
    [Column] public string Name { get; set; }
    [Column] public string? PasswordHash { get; set; }
    [Column] public string? AvatarImage { get; set; }
    [Column] public bool Confirmed { get; set; }
    [Column] public string? ConfirmationCode { get; set; }
    [Column] public bool IsActive { get; set; }
}