namespace Benitoite.Database.Configuration;

public class DatabaseConfig
{
    public string ConnectionString { get; set; }
}