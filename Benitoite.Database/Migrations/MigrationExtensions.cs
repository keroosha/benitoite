using FluentMigrator.Builders.Create;
using FluentMigrator.Builders.Create.Table;

namespace Benitoite.Database.Migrations;

public static class MigrationExtensions
{
    public static ICreateTableColumnOptionOrWithColumnSyntax TableWithGuidId
        (this ICreateExpressionRoot root, string name) => root
        .Table(name)
        .WithColumn("Id").AsGuid().PrimaryKey();
}