using System;
using System.Reflection;
using FluentMigrator;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Initialization;
using Microsoft.Extensions.DependencyInjection;

namespace Benitoite.Database.Migrations
{
    public class MigrationDateAttribute : MigrationAttribute
    {
        private static readonly DateTime UnixEpoch = new(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public MigrationDateAttribute(int year, int month, int day, int hour, int minute, int second = 0)
            : base(Convert(new DateTime(year, month, day, hour, minute, second, DateTimeKind.Utc)))
        {
        }

        private static long Convert(DateTime dt)
        {
            return (long)(dt - UnixEpoch).TotalSeconds;
        }
    }

    public static class MigrationRunnerExtensions
    {
        public static void MigrateDatabase(this IServiceProvider services)
        {
            using var scope = services.CreateScope();
            var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
            using var migrationsScope = runner.BeginScope();
            runner.MigrateUp();
            migrationsScope.Complete();
        }
        
        public static IServiceCollection AddMigrations(this IServiceCollection services, string connectionString)
        {
            return services.AddFluentMigratorCore()
                .ConfigureRunner(runner =>
                {
                    runner.AddPostgres()
                        .WithGlobalConnectionString(connectionString)
                        .ScanIn(typeof(AppDbContext).Assembly);
                })
                .Configure<RunnerOptions>(options => { options.TransactionPerSession = true; })
                .AddLogging(logging => { logging.AddFluentMigratorConsole(); });
        }
    }
}