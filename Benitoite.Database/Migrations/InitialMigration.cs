using FluentMigrator;

namespace Benitoite.Database.Migrations;

[MigrationDate(2022, 12, 23, 21, 16)]
public class InitialMigration : AutoReversingMigration
{
    public override void Up()
    {
        Create.TableWithGuidId("RegistrationRequest")
            .WithColumn("Email").AsString().NotNullable().Unique()
            .WithColumn("Name").AsString().NotNullable()
            .WithColumn("Status").AsString().NotNullable().WithDefaultValue("New");

        Create.TableWithGuidId("Users")
            .WithColumn("Email").AsString().Nullable().Unique()
            .WithColumn("PasswordHash").AsString().Nullable()
            .WithColumn("AvatarImage").AsString().Nullable()
            .WithColumn("Confirmed").AsBoolean().WithDefaultValue(false)
            .WithColumn("ConfirmationCode").AsString().Nullable()
            .WithColumn("Name").AsString()
            .WithColumn("IsActive").AsBoolean().WithDefaultValue(false);
    }
}