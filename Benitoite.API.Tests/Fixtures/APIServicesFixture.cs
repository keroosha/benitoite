using System;
using Benitoite.Database.Migrations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Benitoite.API.Tests.Fixtures;

public class APIServicesFixture : IDisposable
{

    private readonly ServiceProvider service;

    public APIServicesFixture()
    {
        var configuration = new ConfigurationBuilder()
            .AddJsonFile("config.test.json")
            .Build();

        var serviceCollection = new ServiceCollection().InitializeServices(configuration);
        service = serviceCollection.BuildServiceProvider();
        service.MigrateDatabase();
    }
    
    public void Dispose()
    {
    }
}