namespace Benitoite.API.GQL;

public class Query
{
    public QueryV0 V0 { get; set; } = new();
}

public class QueryV0
{
    public string Ping() => "Pong";
}