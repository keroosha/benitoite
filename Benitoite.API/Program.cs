using Benitoite.API.GQL;
using Benitoite.Database;
using Benitoite.Database.Configuration;
using Benitoite.Database.Migrations;
using FluentMigrator.Runner;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);

builder.Services.InitializeServices(builder.Configuration);
builder.Services.AddGraphQLServer()
    .AddType<QueryV0>()
    .AddQueryType<Query>();

var app = builder.Build();

app.Services.MigrateDatabase();

app.UseHttpsRedirection();
app.MapGraphQL();
app.Run();

/// <summary>
/// Integration testing helper
/// </summary>
public static class APIServiceBuilder
{
    public static IServiceCollection InitializeServices(this IServiceCollection services, IConfiguration configuration)
    {
        var databaseConfigSection = configuration.GetSection("Database");
        var databaseConfig = new DatabaseConfig();
        databaseConfigSection.Bind(databaseConfig);
        
        return services
            .Configure<DatabaseConfig>(databaseConfigSection)
            .AddSingleton<IAppDbConnectionFactory, AppDbConnectionFactory>()
            .AddSingleton<AppDbContextManager>()
            .AddMigrations(databaseConfig.ConnectionString);
    }
}